const privates = Symbol('privates');

class 生徒集合 {
    /**
     * @param {生徒[]} items
     */
    constructor(items) {
        // 配列のコピーを保持する。
        this[privates] = { items: [...items] };
    }

    /**
     * @returns {number}
     */
    平均評価点() {
        /** @type {生徒[]} */
        const students = this[privates].items;

        let total = 0;
        for (let student of students) {
            total += student.評価点;
        }

        return Math.round(total / students.length);
    }

    /**
     * @returns {生徒}
     */
    最高成績者() {
        /** @type {生徒[]} */
        const students = [...this[privates].items];

        students.sort((a, b) => {
            if (a.評価点 === b.評価点) return 0;
            return a.評価点 < b.評価点  ? 1 : -1;
        });

        return students[0];
    }
}

module.exports = 生徒集合;