const privates = Symbol('privates');

class 生徒 {
    /**
     * @param {string} 名前
     * @param {number} 評価点
     */
    constructor(名前, 評価点) {
        this[privates] = { 名前, 評価点 };
    }

    /**
     * @returns {string}
     */
    get 名前() {
        return this[privates].名前;
    }

    /**
     * @returns {number}
     */
    get 評価点() {
        return this[privates].評価点
    }
}

module.exports = 生徒;