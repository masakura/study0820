(() => {
    'use strict';

    const random = () => Math.floor(Math.random() * 101);

    const 一年二組 = {
        年: 1,
        組: 2,
        students: [
            { 名前: '平野 雄一郎', 評価点: random() },
            { 名前: '松崎 正道', 評価点: random() },
            { 名前: '黒木 大和', 評価点: random() },
            { 名前: '本郷 晃弘', 評価点: random() },
            { 名前: '沖野 和真', 評価点: random() },
            { 名前: '江村 優作', 評価点: random() },
            { 名前: '谷津 清一', 評価点: random() },
            { 名前: '黒島 政志', 評価点: random() },
            { 名前: '佐合 大輝', 評価点: random() },
        ],
    };

    let total = 0;
    for (const student of 一年二組.students) {
        total += student.評価点;
    }
    const average = Math.round(total / 一年二組.students.length);
    console.log(`平均評価点は ${average} 点です。`);

    const students = [...一年二組.students];
    students.sort((a, b) => {
        if (a.評価点 === b.評価点) return 0;
        return a.評価点 < b.評価点 ? 1 : -1;
    });
    const 最高成績者 = students[0];
    console.log(`最高成績は${最高成績者.名前}さんで ${最高成績者.評価点} 点です。`);
})();
