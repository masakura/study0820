const privates = Symbol('privates');

/**
 * ユーザーの不変オブジェクト。
 */
class ImmutableUser {
    constructor(name) {
        this[privates] = { name };
    }

    /**
     * @returns {string}
     */
    get name() {
        return this[privates].name;
    }
}

module.exports = ImmutableUser;